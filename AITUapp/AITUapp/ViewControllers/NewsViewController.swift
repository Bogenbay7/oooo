//
//  NewsViewController.swift
//  AITUapp
//
//  Created by Nurba on 22.04.2021.
//

import UIKit
import Alamofire
import Kingfisher

class NewsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    let newsUrl = "https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=49afc67ad1964d69b683c966a4ffb4f8"

    var decoder: JSONDecoder = JSONDecoder()
    
    var newsData: [News.Article] = [News.Article]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(NewsTableViewCell.nib, forCellReuseIdentifier: NewsTableViewCell.identifier)
        getData()

 
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        newsData = CoreDataManager.shared.allNews()
    }
    

    func getData(){
        AF.request(newsUrl, method: .get, parameters: [:]).responseJSON { (data) in
            switch data.result {
            case .success:
                if let  result = data.data{
                    do{
                        let News = try JSONDecoder().decode(News.self, from: result)
                        self.newsData += News.articles
                        
                    }catch{
                        print(error)
                    }
                }
            case .failure:
                print("error")
            }
        
        }
    }
 

}

extension NewsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: NewsTableViewCell.identifier, for: indexPath)
as! NewsTableViewCell
        cell.news = newsData[indexPath.row]
        
        return cell
    }
    
    
}
