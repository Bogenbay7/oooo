//
//  LoginViewController.swift
//  AITUapp
//
//  Created by Nurba on 16.04.2021.
//

import UIKit

class LoginViewController: UIViewController {

    struct Constants {
        static let cornerRadius: CGFloat = 8.0
    }
    
    private let usernameField: UITextField = {
        let field = UITextField()
        field.placeholder = "Username or email"
        field.returnKeyType = .next
        field.leftViewMode = .always
        field.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 0))
        field.autocorrectionType = .no
        field.autocapitalizationType = .none
        field.layer.masksToBounds = true
        field.layer.cornerRadius = Constants.cornerRadius
        field.backgroundColor = .secondarySystemBackground

        return field
    }()
    private let passwordField: UITextField = {
        let field = UITextField()
        field.isSecureTextEntry = true
        field.placeholder = "Password"
        field.returnKeyType = .continue
        field.leftViewMode = .always
        field.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 0))
        field.autocorrectionType = .no
        field.autocapitalizationType = .none
        field.layer.masksToBounds = true
        field.layer.cornerRadius = Constants.cornerRadius
        field.backgroundColor = .secondarySystemBackground
        return field
    }()
    private let loginButton: UIButton = {
        let button = UIButton()
        button.setTitle("Log In", for: .normal)
        button.layer.masksToBounds = true
        button.layer.cornerRadius = Constants.cornerRadius
        button.backgroundColor = .systemBlue
        button.setTitleColor(.white, for: .normal)
        
        return button
    }()

    private let createAccountButton: UIButton = {
        let button = UIButton()
        button.setTitle("New User?Create new account...", for: .normal)
        button.setTitleColor(.label, for: .normal)
        
        return button
    }()

    private let headerView: UIView = {
        let header = UIView()
        header.clipsToBounds = true
        let backgroundImageView = UIImageView(image: UIImage(named: "grad"))
        header.addSubview(backgroundImageView)
        return header
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginButton.addTarget(self, action: #selector(didLoginButton), for: .touchUpInside)
        createAccountButton.addTarget(self, action: #selector(didCreateAccButton), for: .touchUpInside)

        
        usernameField.delegate = self
        passwordField.delegate = self
    AddSubviews()
        view.backgroundColor = .systemBackground
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        headerView.frame = CGRect(x: 0, y: 0.0, width: view.width, height: view.height/3.0)
        
        usernameField.frame = CGRect(x: 25, y: headerView.bottom + 40, width: view.width - 50, height: 52.0)

        passwordField.frame = CGRect(x: 25, y: usernameField.bottom + 10, width: view.width - 50, height: 52.0)
        
        loginButton.frame = CGRect(x: 25, y: passwordField.bottom + 10, width: view.width - 50, height: 52.0)
        createAccountButton.frame = CGRect(x: 25, y: loginButton.bottom + 10, width: view.width - 50, height: 52.0)
        
        configureHeaderView()
    }
    
    private func configureHeaderView(){
        guard  headerView.subviews.count == 1 else {
            return
        }
        guard let backgroundView = headerView.subviews.first else {
            return
        }
        backgroundView.frame = headerView.bounds
        
        let imageView = UIImageView(image: UIImage(named: "logo"))
        headerView.addSubview(imageView)
        imageView.contentMode = .scaleAspectFit
        imageView.frame = CGRect(x: headerView.width/4.0, y: view.safeAreaInsets.top, width: headerView.width/2.0, height: headerView.height - view.safeAreaInsets.top)
    }
    
    
    
    private func AddSubviews(){
        view.addSubview(usernameField)
        view.addSubview(passwordField)
        view.addSubview(loginButton)
        view.addSubview(headerView)
        view.addSubview(createAccountButton)
        
    }

    @objc private func didLoginButton() {
        passwordField.resignFirstResponder()
        usernameField.resignFirstResponder()
        
        guard let usernameEmail = usernameField.text, !usernameEmail.isEmpty, let password = passwordField.text, !password.isEmpty  else {
            return
        }
        
        var username: String?
        var email:String?
        
        if usernameEmail.contains("@"),usernameEmail.contains("."){
            //email
            email = usernameEmail
        }else{
            //username
            username = usernameEmail
            
        }
        
        AuthManager.share.loginUser(username: username, email: email, password: password){ success in
            DispatchQueue.main.async {
                if success {
                    self.dismiss(animated: true, completion: nil)
                }else{
                    let alert = UIAlertController(title: "Error", message: "Logging failed", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Dissmis", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                }
                
            }
        }
    }
    
    
    @objc private func didCreateAccButton() {
        let vc = RegisterViewController()
        vc.title = "Create an Account"
        present(UINavigationController(rootViewController: vc), animated: true)
    }
    
    

}

extension LoginViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        if textField == usernameField{
            passwordField.becomeFirstResponder()
        }else if textField == passwordField{
            didLoginButton()
        }
        return true
    }
}
